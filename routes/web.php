<?php

use App\Http\Controllers\PacienteController;
use App\Http\Controllers\VacunaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('vacunas.index');
});

Route::get('/vacunas', [VacunaController::class, 'index'])->name('vacunas.index');

Route::get('/vacunas/{vacuna}', [VacunaController::class, 'show'])->name('vacunas.show');

Route::get('/pacientes/{paciente}/vacunar', [PacienteController::class, 'vacunar'])->name('pacientes.vacunar');

Route::post('/api/vacunas/crear', [VacunaController::class, 'crear'])->name('vacunas.crear');

Route::get('/api/vacunas/{idPaciente}', [VacunaController::class, 'buscar'])->name('vacunas.buscar');

Route::get('/pacientes/buscador', [PacienteController::class, 'buscador'])->name('pacientes.buscador');

Route::post('/pacientes/busquedaAjax', [PacienteController::class, 'busquedaAjax'])->name('pacientes.busquedaAjax');

