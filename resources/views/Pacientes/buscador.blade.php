@extends('../layouts/master')

@section('contenido')
    <div class="row">
        <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
    </div>
    
    <script>
        $(document).ready(function () {

            $(document).on("keypress", "#busqueda", function (e){
                $.ajax({
                    url: 'busquedaAjax',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    data: {
                        'paciente': $('#busqueda').val()
                    },
                    success: function (data) {
                        $("#busqueda").autocomplete({
                            source: data.map(x => x['nombre'])
                        });
                    }
                });

            });
        });
    </script>
@endsection





