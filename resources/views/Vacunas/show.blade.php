@extends('../layouts/master')

@section('contenido')
<div class="row">
    <div class="col-sm-9">
        <h3>{{$vacuna->nombre}}</h3>
        <h2>Pacientes NO VACUNADOS:</h2>

        <table>
            <tr>
                <th>Nombre</th>
                <th>Grupo de Vacunación</th>
                <th>Prioridad</th>
                <th>Acción</th>
            </tr>
            @foreach($vacuna->grupos as $grupo)
                @foreach($grupo->pacientes->where('vacunado', '0') as $paciente)
                    <tr>
                        <td>{{$paciente->nombre}}</td>
                        <td>{{$grupo->nombre}}</td>
                        <td>{{$grupo->prioridad}}</td>
                        <td><a href="{{ route('pacientes.vacunar' , $paciente) }}">Vacunar</a></td>
                    </tr>
                @endforeach
            @endforeach
        </table>
    </div>
</div>

<br><br><br><br>

<h2>Pacientes Vacunados</h2>
<div class="row" style="display: inline-block">
    @foreach($vacuna->grupos as $grupo)
        <h6>{{$grupo->nombre}}</h6>
        <ul>
            @foreach($grupo->pacientes->where('vacunado', '1') as $paciente)
                <li>{{$paciente->nombre}} ({{$paciente->fechaVacuna}})</li>
            @endforeach
        </ul>
    @endforeach
</div>
@endsection