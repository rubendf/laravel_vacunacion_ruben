@extends('../layouts/master')

@section('contenido')
    <div class="row">
        @foreach($vacunas as $vacuna)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="{{route('vacunas.show' , $vacuna)}}">
                    <h6>{{$vacuna->nombre}}</h6>
                    <ul>
                        @foreach($vacuna->grupos as $grupo)
                        <li>{{$grupo->nombre}}</li>
                        @endforeach
                    </ul>
                </a>
            </div>
        @endforeach
    </div>
@endsection