<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use App\Models\Paciente;
use App\Models\Vacuna;
use Illuminate\Http\Request;

class VacunaController extends Controller
{
    public function index(){
        $vacunas = Vacuna::all();
        return view('Vacunas.index', compact('vacunas'));
    }

    public function show(Vacuna $vacuna){
        return view('Vacunas.show', compact('vacuna'));
    }

    public function crear(Request $request){
        $request['slug'] = $request->all()['nombre'];
        $vacuna = Vacuna::create($request->all());

        if($vacuna->save()){
            return response()->json(['mensaje' => 'La vacuna se ha guardado correctamente']);
        }else{
            return response()->json(['mensaje' => 'Ha ocurrido un error al guardar la vacuna']);
        }
    }

    public function buscar($paciente_id){
        $grupo = Paciente::findOrFail($paciente_id)->grupo_id;
        return response()->json(Grupo::findOrFail($grupo)->vacunas);
    }

    
}
