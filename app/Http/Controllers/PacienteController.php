<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    public function vacunar(Paciente $paciente){
        $paciente->vacunado = 1;
        $paciente->fechaVacuna = date('Y-m-d');
        $paciente->save();

        return back()->with('mensaje', $paciente->nombre . ' ha sido vacunado');
    }

    public function buscador(){
        return view('Pacientes.buscador');
    }

    public function busquedaAjax(Request $request){
        return response()->json(Paciente::select('nombre')->where('nombre', 'like', '%' . $request->paciente . '%')->get());
    }
}
