<?php

namespace Database\Factories;

use App\Models\Grupo;
use App\Models\Model;
use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->name();
        $booleano = $this->faker->boolean();
        return [
            'nombre' =>  $nombre,
            'slug' => Str::slug($nombre),
            'vacunado' => $booleano,
            'grupo_id' => Grupo::all()->random()->id,
            'fechaVacuna' => $booleano ? $this->faker->dateTimeThisYear() : NULL,
        ];
    }
}
